package com.foriseland.social.socialRong.utils.rong.base.impl;

import com.foriseland.social.socialRong.utils.rong.base.BaseUtils;
import com.foriseland.social.socialRong.utils.rong.base.OthersUtils;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.CodeSuccessResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.ListWordfilterResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.PushMessage;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.SMSImageCodeResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.SMSSendCodeResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.SMSVerifyCodeResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.UserTag;

/**
 * 其他服务
 * @author sjm
 *
 */
public class OthersUtilsImpl extends BaseUtils implements OthersUtils{

	public String add(String word) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.wordfilter.add(word);
		return checkResult(result);
	}

	public String getList() throws Exception {
		ListWordfilterResult result = rongCloud.wordfilter.getList();
		return checkResult(result);
	}

	public String delete(String word) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.wordfilter.delete(word);
		return checkResult(result);
	}

	public String batchDelete(String[] words) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.wordfilter.batchDelete(words);
		return checkResult(result);
	}

	public String setUserPushTag(UserTag userTag) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.push.setUserPushTag(userTag);
		return checkResult(result);
	}

	public String broadcastPush(PushMessage pushMessage) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.push.broadcastPush(pushMessage);
		return checkResult(result);
	}

	public String getImageCode() throws Exception {
		SMSImageCodeResult result = rongCloud.sms.getImageCode(conf.getString("rong.cloud.appKey"));
		return checkResult(result);
	}

	public String sendCode(String mobile, String templateId, String region, String verifyId, String verifyCode)
			throws IllegalArgumentException, Exception {
		SMSSendCodeResult result = rongCloud.sms.sendCode(mobile, templateId, region, verifyId, verifyCode);
		return checkResult(result);
	}

	public String verifyCode(String sessionId, String code) throws IllegalArgumentException, Exception {
		SMSVerifyCodeResult result = rongCloud.sms.verifyCode(sessionId, code);
		return checkResult(result);
	}
	
}
