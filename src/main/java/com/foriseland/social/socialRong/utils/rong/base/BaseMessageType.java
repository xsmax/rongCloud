package com.foriseland.social.socialRong.utils.rong.base;

public enum BaseMessageType {
	RC_VCMSG("RC:VcMsg"),
	RC_TXTMSG("RC:TxtMsg"),
	RC_CMDNTF("RC:CmdNtf"),
	RC_CONTACTNTF("RC:ContactNtf"),
	RC_IMGMSG("RC:ImgMsg"),
	RC_IMGTEXTMSG("RC:ImgTextMsg"),
	RC_InfoNtf("RC:InfoNtf");
	
	
	
	
	private String type;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	private BaseMessageType(String type){
		this.type = type;
	}
}
