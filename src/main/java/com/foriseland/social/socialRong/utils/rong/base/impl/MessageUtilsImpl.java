package com.foriseland.social.socialRong.utils.rong.base.impl;

import com.foriseland.social.socialRong.utils.rong.base.BaseUtils;
import com.foriseland.social.socialRong.utils.rong.base.MessageUtils;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.CodeSuccessResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.ConversationGetResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.HistoryMessageResult;

public class MessageUtilsImpl extends BaseUtils implements MessageUtils {

	public String getHistory(String date) throws IllegalArgumentException, Exception {
		HistoryMessageResult result = rongCloud.message.getHistory(date);
		return checkResult(result);
	}

	public String deleteMessage(String date) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.message.deleteMessage(date);
		return checkResult(result);
	}


	public String set(String conversationType, String requestId, String targetId, Integer isMuted)
			throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.conversation.set(conversationType, requestId, targetId, isMuted);
		return checkResult(result);
	}

	public String get(String conversationType, String requestId, String targetId)
			throws IllegalArgumentException, Exception {
		ConversationGetResult result = rongCloud.conversation.get(conversationType, requestId, targetId);
		return checkResult(result);
	}

}
