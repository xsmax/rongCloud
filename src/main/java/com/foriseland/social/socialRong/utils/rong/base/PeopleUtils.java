package com.foriseland.social.socialRong.utils.rong.base;

import com.foriseland.social.socialRong.utils.thirdparty.io.rong.messages.BaseMessage;

/**
 * 抽象消息工具类
 * 
 * @author sjm
 *
 */
public interface PeopleUtils{

	// 私信
	/**
	 * 发送单聊消息方法（一个用户向另外一个用户发送消息，单条消息最大 128k。每分钟最多发送 6000 条信息，每次发送用户上限为 1000
	 * 人，如：一次发送 1000 人时，示为 1000 条消息。）
	 * 
	 * @param fromUserId
	 *            发送人用户 Id。（必传）
	 * @param toUserId
	 *            接收用户 Id，可以实现向多人发送消息，每次上限为 1000 人。（必传）
	 * @param message
	 *            消息。
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String publishPrivate(String fromUserId, String[] toUserId, BaseMessage message)
			throws IllegalArgumentException, Exception;

	/**
	 * 发送单聊消息方法（一个用户向另外一个用户发送消息，单条消息最大 128k。每分钟最多发送 6000 条信息，每次发送用户上限为 1000
	 * 人，如：一次发送 1000 人时，示为 1000 条消息。）
	 * 
	 * @param fromUserId
	 *            发送人用户 Id。（必传）
	 * @param toUserId
	 *            接收用户 Id，可以实现向多人发送消息，每次上限为 1000 人。（必传）
	 * @param message
	 *            消息。
	 * @param pushContent
	 *            定义显示的 Push 内容，如果 objectName 为融云内置消息类型时，则发送后用户一定会收到 Push
	 *            信息。如果为自定义消息，则 pushContent 为自定义消息显示的 Push 内容，如果不传则用户不会收到 Push
	 *            通知。（可选）
	 * @param verifyBlacklist
	 *            是否过滤发送人黑名单列表，0 表示为不过滤、 1 表示为过滤，默认为 0 不过滤。（可选）
	 * @param isPersisted
	 *            当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行存储，0 表示为不存储、 1
	 *            表示为存储，默认为 1 存储消息。（可选）
	 * @param isCounted
	 *            当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行未读消息计数，0 表示为不计数、 1
	 *            表示为计数，默认为 1 计数，未读消息数增加 1。（可选）
	 * @param isIncludeSender
	 *            发送用户自已是否接收消息，0 表示为不接收，1 表示为接收，默认为 0 不接收。（可选）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String publishPrivateExtend(String fromUserId, String[] toUserId, BaseMessage message,
			String pushContent, Integer verifyBlacklist, Integer isPersisted, Integer isCounted,
			Integer isIncludeSender) throws IllegalArgumentException, Exception;

	/**
	 * 发送单聊消息方法（一个用户向另外一个用户发送消息，单条消息最大 128k。每分钟最多发送 6000 条信息，每次发送用户上限为 1000
	 * 人，如：一次发送 1000 人时，示为 1000 条消息。）
	 * 
	 * @param fromUserId
	 *            发送人用户 Id。（必传）
	 * @param toUserId
	 *            接收用户 Id，可以实现向多人发送消息，每次上限为 1000 人。（必传）
	 * @param message
	 *            消息。
	 * @param pushContent
	 *            定义显示的 Push 内容，如果 objectName 为融云内置消息类型时，则发送后用户一定会收到 Push
	 *            信息。如果为自定义消息，则 pushContent 为自定义消息显示的 Push 内容，如果不传则用户不会收到 Push
	 *            通知。（可选）
	 * @param pushData
	 *            针对 iOS 平台为 Push 通知时附加到 payload 中，Android 客户端收到推送消息时对应字段名为
	 *            pushData。（可选）
	 * @param count
	 *            针对 iOS 平台，Push 时用来控制未读消息显示数，只有在 toUserId 为一个用户 Id 的时候有效。（可选）
	 * @param verifyBlacklist
	 *            是否过滤发送人黑名单列表，0 表示为不过滤、 1 表示为过滤，默认为 0 不过滤。（可选）
	 * @param isPersisted
	 *            当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行存储，0 表示为不存储、 1
	 *            表示为存储，默认为 1 存储消息。（可选）
	 * @param isCounted
	 *            当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行未读消息计数，0 表示为不计数、 1
	 *            表示为计数，默认为 1 计数，未读消息数增加 1。（可选）
	 * @param isIncludeSender
	 *            发送用户自已是否接收消息，0 表示为不接收，1 表示为接收，默认为 0 不接收。（可选）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String publishPrivateExtendIOS(String fromUserId, String[] toUserId, BaseMessage message,
			String pushContent, String pushData, String count, Integer verifyBlacklist, Integer isPersisted,
			Integer isCounted, Integer isIncludeSender) throws IllegalArgumentException, Exception;

	// 系统消息

	/**
	 * 发送系统消息方法（一个用户向一个或多个用户发送系统消息，单条消息最大 128k，会话类型为 SYSTEM。每秒钟最多发送 100
	 * 条消息，每次最多同时向 100 人发送，如：一次发送 100 人时，示为 100 条消息。）
	 * 
	 * @param fromUserId
	 *            发送人用户 Id。（必传）
	 * @param toUserId
	 *            接收用户 Id，提供多个本参数可以实现向多人发送消息，上限为 1000 人。（必传）
	 * @param message
	 *            发送消息内容（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String PublishSystem(String fromUserId, String[] toUserId, BaseMessage message)
			throws IllegalArgumentException, Exception;

	/**
	 * 发送系统消息方法（一个用户向一个或多个用户发送系统消息，单条消息最大 128k，会话类型为 SYSTEM。每秒钟最多发送 100
	 * 条消息，每次最多同时向 100 人发送，如：一次发送 100 人时，示为 100 条消息。）
	 * 
	 * @param fromUserId
	 *            发送人用户 Id。（必传）
	 * @param toUserId
	 *            接收用户 Id，提供多个本参数可以实现向多人发送消息，上限为 1000 人。（必传）
	 * @param message
	 *            发送消息内容（必传）
	 * @param pushContent
	 *            如果为自定义消息，定义显示的 Push 内容，内容中定义标识通过 values
	 *            中设置的标识位内容进行替换.如消息类型为自定义不需要 Push 通知，则对应数组传空值即可。（可选）
	 * @param isPersisted
	 *            当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行存储，0 表示为不存储、 1
	 *            表示为存储，默认为 1 存储消息。（可选）
	 * @param isCounted
	 *            当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行未读消息计数，0 表示为不计数、 1
	 *            表示为计数，默认为 1 计数，未读消息数增加 1。（可选）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String PublishSystemExtend(String fromUserId, String[] toUserId, BaseMessage message,
			String pushContent, Integer isPersisted, Integer isCounted) throws IllegalArgumentException, Exception;

	/**
	 * 发送系统消息方法（一个用户向一个或多个用户发送系统消息，单条消息最大 128k，会话类型为 SYSTEM。每秒钟最多发送 100
	 * 条消息，每次最多同时向 100 人发送，如：一次发送 100 人时，示为 100 条消息。）
	 * 
	 * @param fromUserId
	 *            发送人用户 Id。（必传）
	 * @param toUserId
	 *            接收用户 Id，提供多个本参数可以实现向多人发送消息，上限为 1000 人。（必传）
	 * @param message
	 *            发送消息内容（必传）
	 * @param pushContent
	 *            如果为自定义消息，定义显示的 Push 内容，内容中定义标识通过 values
	 *            中设置的标识位内容进行替换.如消息类型为自定义不需要 Push 通知，则对应数组传空值即可。（可选）
	 * @param pushData
	 *            针对 iOS 平台为 Push 通知时附加到 payload 中，Android 客户端收到推送消息时对应字段名为
	 *            pushData。如不需要 Push 功能对应数组传空值即可。（可选）
	 * @param isPersisted
	 *            当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行存储，0 表示为不存储、 1
	 *            表示为存储，默认为 1 存储消息。（可选）
	 * @param isCounted
	 *            当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行未读消息计数，0 表示为不计数、 1
	 *            表示为计数，默认为 1 计数，未读消息数增加 1。（可选）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String PublishSystemExtendIOS(String fromUserId, String[] toUserId, BaseMessage message,
			String pushContent, String pushData, Integer isPersisted, Integer isCounted)
			throws IllegalArgumentException, Exception;

}
