package com.foriseland.social.socialRong.utils.rong.base;

import com.foriseland.social.socialRong.utils.thirdparty.io.rong.messages.BaseMessage;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.ChatRoomInfo;

public interface ChatRoomUtils {
	
	
	/**
	 * 发送聊天室消息方法（一个用户向聊天室发送消息，单条消息最大 128k。每秒钟限 100 次。）
	 * @param userId 发送人用户 Id。（必传）
	 * @param toChatroomId 接收聊天室Id，提供多个本参数可以实现向多个聊天室发送消息。（必传）
	 * @param message 发送消息内容（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String publishChatroom(String userId,String[] toChatroomId,BaseMessage message) throws IllegalArgumentException,Exception;

	
	/**
	 * 发送广播消息方法（发送消息给一个应用下的所有注册用户，如用户未在线会对满足条件（绑定手机终端）的用户发送 Push 信息，单条消息最大 128k，会话类型为 SYSTEM。每小时只能发送 1 次，每天最多发送 3 次。）
	 * @param fromUserId 发送人用户 Id。（必传）
	 * @param message 文本消息。（必传）
	 * @return  String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String broadcast(String fromUserId, BaseMessage message) throws IllegalArgumentException,Exception;
	

	
	/**
	 * 发送广播消息方法（发送消息给一个应用下的所有注册用户，如用户未在线会对满足条件（绑定手机终端）的用户发送 Push 信息，单条消息最大 128k，会话类型为 SYSTEM。每小时只能发送 1 次，每天最多发送 3 次。）
	 * @param fromUserId 发送人用户 Id。（必传）
	 * @param message 文本消息。
	 * @param pushContent 定义显示的 Push 内容，如果 objectName 为融云内置消息类型时，则发送后用户一定会收到 Push 信息. 如果为自定义消息，则 pushContent 为自定义消息显示的 Push 内容，如果不传则用户不会收到 Push 通知.（可选）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String broadcastExtend(String fromUserId, BaseMessage message, String pushContent) throws IllegalArgumentException,Exception;
	
	/**
	 * 发送广播消息方法（发送消息给一个应用下的所有注册用户，如用户未在线会对满足条件（绑定手机终端）的用户发送 Push 信息，单条消息最大 128k，会话类型为 SYSTEM。每小时只能发送 1 次，每天最多发送 3 次。） 
	 * 
	 * @param  fromUserId:发送人用户 Id。（必传）
	 * @param  txtMessage:文本消息。
	 * @param  pushContent:定义显示的 Push 内容，如果 objectName 为融云内置消息类型时，则发送后用户一定会收到 Push 信息. 如果为自定义消息，则 pushContent 为自定义消息显示的 Push 内容，如果不传则用户不会收到 Push 通知.（可选）
	 * @param  pushData:针对 iOS 平台为 Push 通知时附加到 payload 中，Android 客户端收到推送消息时对应字段名为 pushData。（可选）
	 * @param  os:针对操作系统发送 Push，值为 iOS 表示对 iOS 手机用户发送 Push ,为 Android 时表示对 Android 手机用户发送 Push ，如对所有用户发送 Push 信息，则不需要传 os 参数。（可选）
	 *
	 * @return CodeSuccessResult
	 **/
	
	/**
	 * 发送广播消息方法（发送消息给一个应用下的所有注册用户，如用户未在线会对满足条件（绑定手机终端）的用户发送 Push 信息，单条消息最大 128k，会话类型为 SYSTEM。每小时只能发送 1 次，每天最多发送 3 次。）
	 * @param fromUserId 发送人用户 Id。（必传）
	 * @param message 文本消息。
	 * @param pushContent 定义显示的 Push 内容，如果 objectName 为融云内置消息类型时，则发送后用户一定会收到 Push 信息. 如果为自定义消息，则 pushContent 为自定义消息显示的 Push 内容，如果不传则用户不会收到 Push 通知.（可选）
	 * @param pushData 针对 iOS 平台为 Push 通知时附加到 payload 中，Android 客户端收到推送消息时对应字段名为 pushData。（可选）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String broadcastExtendIOS(String fromUserId, BaseMessage message, String pushContent, String pushData) throws IllegalArgumentException,Exception;
	

	
	/**
	 * 发送广播消息方法（发送消息给一个应用下的所有注册用户，如用户未在线会对满足条件（绑定手机终端）的用户发送 Push 信息，单条消息最大 128k，会话类型为 SYSTEM。每小时只能发送 1 次，每天最多发送 3 次。）
	 * @param fromUserId 发送人用户 Id。（必传）
	 * @param message 文本消息。
	 * @param pushContent 定义显示的 Push 内容，如果 objectName 为融云内置消息类型时，则发送后用户一定会收到 Push 信息. 如果为自定义消息，则 pushContent 为自定义消息显示的 Push 内容，如果不传则用户不会收到 Push 通知.（可选）
	 * @param os 针对操作系统发送 Push，值为 iOS 表示对 iOS 手机用户发送 Push ,为 Android 时表示对 Android 手机用户发送 Push ，如对所有用户发送 Push 信息，则不需要传 os 参数。（可选）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String broadcastExtendOS(String fromUserId, BaseMessage message, String pushContent, String os) throws IllegalArgumentException,Exception;
	
	
	/**
	 * 发送广播消息方法（发送消息给一个应用下的所有注册用户，如用户未在线会对满足条件（绑定手机终端）的用户发送 Push 信息，单条消息最大 128k，会话类型为 SYSTEM。每小时只能发送 1 次，每天最多发送 3 次。）
	 * @param fromUserId 发送人用户 Id。（必传）
	 * @param message 文本消息。
	 * @param pushContent 定义显示的 Push 内容，如果 objectName 为融云内置消息类型时，则发送后用户一定会收到 Push 信息. 如果为自定义消息，则 pushContent 为自定义消息显示的 Push 内容，如果不传则用户不会收到 Push 通知.（可选）
	 * @param pushData 针对 iOS 平台为 Push 通知时附加到 payload 中，Android 客户端收到推送消息时对应字段名为 pushData。（可选）
	 * @param os 针对操作系统发送 Push，值为 iOS 表示对 iOS 手机用户发送 Push ,为 Android 时表示对 Android 手机用户发送 Push ，如对所有用户发送 Push 信息，则不需要传 os 参数。（可选）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String broadcastExtendALL(String fromUserId, BaseMessage message, String pushContent, String pushData, String os) throws IllegalArgumentException,Exception;
	
	/**
	 * 创建聊天室方法 
	 * @param chatRoomInfo id:要创建的聊天室的id；name:要创建的聊天室的name。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String create(ChatRoomInfo[] chatRoomInfo) throws IllegalArgumentException,Exception;
	
	/**
	 * 加入聊天室方法  
	 * @param userId 要加入聊天室的用户 Id，可提交多个，最多不超过 50 个。（必传）
	 * @param chatroomId 要加入的聊天室 Id。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String join(String[] userId, String chatroomId) throws IllegalArgumentException,Exception;
	
	/**
	 * 查询聊天室信息方法 
	 * @param chatroomId 要查询的聊天室id（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String query(String[] chatroomId) throws IllegalArgumentException,Exception;
	
	/**
	 * 查询聊天室内用户方法 
	 * @param chatroomId 要查询的聊天室 ID。（必传）
	 * @param count 要获取的聊天室成员数，上限为 500 ，超过 500 时最多返回 500 个成员。（必传）
	 * @param order 加入聊天室的先后顺序， 1 为加入时间正序， 2 为加入时间倒序。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String queryUser(String chatroomId, String count, String order) throws IllegalArgumentException,Exception;
	
	
	/**
	 * 聊天室消息停止分发方法（可实现控制对聊天室中消息是否进行分发，停止分发后聊天室中用户发送的消息，融云服务端不会再将消息发送给聊天室中其他用户。）
	 * @param chatroomId 聊天室 Id。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String stopDistributionMessage(String chatroomId) throws IllegalArgumentException,Exception;
	
	/**
	 * 聊天室消息恢复分发方法 
	 * @param chatroomId  聊天室 Id。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String resumeDistributionMessage(String chatroomId) throws IllegalArgumentException,Exception;
	
	/**
	 * 添加禁言聊天室成员方法（在 App 中如果不想让某一用户在聊天室中发言时，可将此用户在聊天室中禁言，被禁言用户可以接收查看聊天室中用户聊天信息，但不能发送消息.）
	 * @param userId 用户 Id。（必传）
	 * @param chatroomId 聊天室 Id。（必传）
	 * @param minute 禁言时长，以分钟为单位，最大值为43200分钟。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String addGagUser(String userId, String chatroomId, String minute) throws IllegalArgumentException,Exception;
	
	/**
	 * 查询被禁言聊天室成员方法 
	 * @param chatroomId 聊天室 Id。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String ListGagUser(String chatroomId) throws IllegalArgumentException,Exception;
	
	/**
	 * 移除禁言聊天室成员方法  
	 * @param userId 用户 Id。（必传）
	 * @param chatroomId 聊天室Id。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String rollbackGagUser(String userId, String chatroomId) throws IllegalArgumentException,Exception;
	
	/**
	 * 添加封禁聊天室成员方法 
	 * @param userId  用户 Id。（必传）
	 * @param chatroomId 聊天室 Id。（必传）
	 * @param minute 封禁时长，以分钟为单位，最大值为43200分钟。（必传）
	 * @return
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String addBlockUser(String userId, String chatroomId, String minute) throws IllegalArgumentException,Exception;
	
	/**
	 * 查询被封禁聊天室成员方法 
	 * @param chatroomId 聊天室 Id。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String getListBlockUser(String chatroomId) throws IllegalArgumentException,Exception;
	
	/**
	 * 移除封禁聊天室成员方法
	 * @param userId 用户 Id。（必传）
	 * @param chatroomId 聊天室 Id。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String rollbackBlockUser(String userId, String chatroomId) throws IllegalArgumentException,Exception;
	
	/**
	 * 添加聊天室消息优先级方法 
	 * @param objectName 低优先级的消息类型，每次最多提交 5 个，设置的消息类型最多不超过 20 个。（必传）
	 * @return
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String addPriority(String[] objectName) throws IllegalArgumentException,Exception;
	
	/**
	 * 移除聊天室消息優先級方法
	 * @param objectName 低优先级的消息类型，每次最多提交 5 个
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String rollbackPriority(String[] objectName) throws IllegalArgumentException,Exception;
	
	/**
	 * 查詢聊天室消息優先級方法
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String queryPriority() throws IllegalArgumentException,Exception;
	
	/**
	 * 销毁聊天室方法 
	 * @param chatroomId 要销毁的聊天室 Id。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String destroy(String[] chatroomId) throws IllegalArgumentException,Exception;
	
	
	/**
	 * 添加聊天室白名单成员方法 
	 * @param chatroomId 聊天室中用户 Id，可提交多个，聊天室中白名单用户最多不超过 5 个。（必传）
	 * @param userId 聊天室 Id。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String addWhiteListUser(String chatroomId, String[] userId) throws IllegalArgumentException,Exception;
	
	/**
	 *  移除聊天室白名单成员方法
	 * @param chatroomId 聊天室 Id。（必传）
	 * @param userId 聊天室白名单中用户 Id，可提交多个，最多不超过 5 个。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String removeWhiteListUser(String chatroomId, String[] userId) throws IllegalArgumentException,Exception;
	
	/**
	 * 查询聊天室白名单成员方法
	 * @param chatroomId 聊天室 Id。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String queryWhiteListUser(String chatroomId) throws IllegalArgumentException,Exception;
	/**
	 * 添加聊天室全局禁言方法
	 * @param userId 用户 Id，可同时禁言多个用户，建议最多不超过 20 个。（必传）
	 * @param minute 禁言时长，以分钟为单位，最大值为 43200 分钟。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String addGlobalGag(String[] userId,String minute) throws IllegalArgumentException,Exception;
	
	/**
	 * 移除聊天室全局禁言方法
	 * @param userId 用户 Id，可同时移除多个用户，建议最多不超过 20 个。（必传）
	 * @return
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String rollbackGlobalGag(String[] userId) throws IllegalArgumentException,Exception;
	
	/**
	 * 查詢聊天室全局禁言用戶的方法
	 * @return String
	 * @throws Exception
	 */
	public String queryGlobalGag() throws Exception;
} 
