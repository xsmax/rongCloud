package com.foriseland.social.socialRong.utils.rong.base;

public interface MessageUtils {
	
	/**
	 * 消息历史记录下载地址获取 方法消息历史记录下载地址获取方法。获取 APP 内指定某天某小时内的所有会话消息记录的下载地址。（目前支持二人会话、讨论组、群组、聊天室、客服、系统通知消息历史记录下载）
	 * @param date 指定北京时间某天某小时，格式为2014010101,表示：2014年1月1日凌晨1点。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String getHistory(String date) throws IllegalArgumentException,Exception;
	
	/**
	 * 消息历史记录删除方法（删除 APP 内指定某天某小时内的所有会话消息记录。调用该接口返回成功后，date参数指定的某小时的消息记录文件将在随后的5-10分钟内被永久删除。）
	 * @param date 指定北京时间某天某小时，格式为2014010101,表示：2014年1月1日凌晨1点。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String deleteMessage(String date) throws IllegalArgumentException,Exception;
	
	/**
	 * 设置会话消息免打扰方法
	 * @param conversationType 会话类型，二人会话是 1 、讨论组会话是 2 、群组会话是 3 、客服会话是 5 、系统通知是 6 、应用公众服务是 7 、公众服务是 8 。（必传）
	 * @param requestId 设置消息免打扰的用户 Id。（必传）
	 * @param targetId 目标 Id，根据不同的 ConversationType，可能是用户 Id、讨论组 Id、群组 Id、客服 Id、公众号 Id。（必传）
	 * @param isMuted 消息免打扰设置状态，0 表示为关闭，1 表示为开启。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String set(String conversationType,String requestId,String targetId,Integer isMuted) throws IllegalArgumentException,Exception;
	
	/**
	 * 查询用户某一会话消息免打扰的设置状态。
	 * @param conversationType 会话类型，二人会话是 1 、讨论组会话是 2 、群组会话是 3 、客服会话是 5 、系统通知是 6 、应用公众服务是 7 、公众服务是 8 。（必传）
	 * @param requestId 设置消息免打扰的用户 Id。（必传）
	 * @param targetId 目标 Id，根据不同的 ConversationType，可能是用户 Id、讨论组 Id、群组 Id、客服 Id、公众号 Id。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String get(String conversationType,String requestId,String targetId) throws IllegalArgumentException,Exception;
}
