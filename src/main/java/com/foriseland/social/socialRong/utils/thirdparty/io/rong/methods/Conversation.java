package com.foriseland.social.socialRong.utils.thirdparty.io.rong.methods;

import java.net.HttpURLConnection;
import java.net.URLEncoder;

import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.CodeSuccessResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.ConversationGetResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.util.GsonUtil;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.util.HostType;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.util.HttpUtil;

public class Conversation {
	private static final String UTF8 = "UTF-8";
	private String appKey;
	private String appSecret;
	public Conversation(String appKey, String appSecret) {
		this.appKey = appKey;
		this.appSecret = appSecret;
	}
	
	/**
	 * 查询用户某一会话消息免打扰的设置状态。
	 * @param conversationType 会话类型，二人会话是 1 、讨论组会话是 2 、群组会话是 3 、客服会话是 5 、系统通知是 6 、应用公众服务是 7 、公众服务是 8 。（必传）
	 * @param requestId 设置消息免打扰的用户 Id。（必传）
	 * @param targetId 目标 Id，根据不同的 ConversationType，可能是用户 Id、讨论组 Id、群组 Id、客服 Id、公众号 Id。（必传）
	 * @return  ConversationGetResult
	 * @throws Exception
	 */
	public ConversationGetResult get(String conversationType,String requestId,String targetId) throws Exception {
		if (conversationType == null) {
			throw new IllegalArgumentException("Paramer 'conversationType' is required");
		}
		if (requestId == null) {
			throw new IllegalArgumentException("Paramer 'requestId' is required");
		}
		if (targetId == null) {
			throw new IllegalArgumentException("Paramer 'targetId' is required");
		}
	   	StringBuilder sb = new StringBuilder();
	    sb.append("&conversationType=").append(URLEncoder.encode(conversationType.toString(), UTF8));
	    sb.append("&requestId=").append(URLEncoder.encode(requestId.toString(), UTF8));
	    sb.append("&targetId=").append(URLEncoder.encode(targetId.toString(), UTF8));
		
	   	String body = sb.toString();
	   	if (body.indexOf("&") == 0) {
	   		body = body.substring(1, body.length());
	   	}
	   	
	   	HttpURLConnection conn = HttpUtil.CreatePostHttpConnection(HostType.API, appKey, appSecret, "/conversation/notification/get.json", "application/x-www-form-urlencoded");
	   	HttpUtil.setBodyParameter(body, conn);
	    
	    return (ConversationGetResult) GsonUtil.fromJson(HttpUtil.returnResult(conn), ConversationGetResult.class);
	}
	/**
	 * 设置会话消息免打扰方法
	 * @param conversationType 会话类型，二人会话是 1 、讨论组会话是 2 、群组会话是 3 、客服会话是 5 、系统通知是 6 、应用公众服务是 7 、公众服务是 8 。（必传）
	 * @param requestId 设置消息免打扰的用户 Id。（必传）
	 * @param targetId 目标 Id，根据不同的 ConversationType，可能是用户 Id、讨论组 Id、群组 Id、客服 Id、公众号 Id。（必传）
	 * @param isMuted 消息免打扰设置状态，0 表示为关闭，1 表示为开启。（必传）
	 * @return  CodeSuccessResult
	 * @throws Exception
	 */
	public CodeSuccessResult set(String conversationType,String requestId,String targetId,Integer isMuted) throws Exception {
		if (conversationType == null) {
			throw new IllegalArgumentException("Paramer 'conversationType' is required");
		}
		if (requestId == null) {
			throw new IllegalArgumentException("Paramer 'requestId' is required");
		}
		if (targetId == null) {
			throw new IllegalArgumentException("Paramer 'targetId' is required");
		}
		if (isMuted == null) {
			throw new IllegalArgumentException("Paramer 'isMuted' is required");
		}
	   	StringBuilder sb = new StringBuilder();
	    sb.append("&conversationType=").append(URLEncoder.encode(conversationType.toString(), UTF8));
	    sb.append("&requestId=").append(URLEncoder.encode(requestId.toString(), UTF8));
	    sb.append("&targetId=").append(URLEncoder.encode(targetId.toString(), UTF8));
	    sb.append("&isMuted=").append(URLEncoder.encode(isMuted.toString(), UTF8));
		
	   	String body = sb.toString();
	   	if (body.indexOf("&") == 0) {
	   		body = body.substring(1, body.length());
	   	}
	   	
	   	HttpURLConnection conn = HttpUtil.CreatePostHttpConnection(HostType.API, appKey, appSecret, "/conversation/notification/set.json", "application/x-www-form-urlencoded");
	   	HttpUtil.setBodyParameter(body, conn);
	    
	    return (CodeSuccessResult) GsonUtil.fromJson(HttpUtil.returnResult(conn), CodeSuccessResult.class);
	}
}
