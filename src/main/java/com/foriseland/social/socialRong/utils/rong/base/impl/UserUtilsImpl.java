package com.foriseland.social.socialRong.utils.rong.base.impl;

import com.foriseland.social.socialRong.utils.rong.base.BaseUtils;
import com.foriseland.social.socialRong.utils.rong.base.UserUtils;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.CheckOnlineResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.CodeSuccessResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.QueryBlacklistUserResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.QueryBlockUserResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.TokenResult;

/**
 * 用户工具 
 * 
 * @author sjm
 *
 */
public class UserUtilsImpl extends BaseUtils implements UserUtils {

	public String getToken(String userId, String userName, String portraitUri)
			throws IllegalArgumentException, Exception {
		TokenResult result = rongCloud.user.getToken(userId, userName, portraitUri);
		return checkResult(result);
	}

	public String refresh(String userId, String userName, String portraitUri)
			throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.user.refresh(userId, userName, portraitUri);
		return checkResult(result);

	}

	public String checkOnline(String userId) throws IllegalArgumentException, Exception {
		CheckOnlineResult result = rongCloud.user.checkOnline(userId);
		return checkResult(result);
	}

	public String block(String userId, Integer minute) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.user.block(userId, minute);
		return checkResult(result);
	}

	public String unBlock(String userId) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.user.unBlock(userId);
		return checkResult(result);
	}

	public String queryBlock() throws Exception {
		QueryBlockUserResult result = rongCloud.user.queryBlock();
		return checkResult(result);
	}

	public String addBlacklist(String userId, String blackUserId) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.user.addBlacklist(userId, blackUserId);
		return checkResult(result);
	}

	public String queryBlacklist(String userId) throws IllegalArgumentException, Exception {
		QueryBlacklistUserResult result = rongCloud.user.queryBlacklist(userId);
		return checkResult(result);
	}

	public String removeBlacklist(String userId, String blackUserId) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.user.removeBlacklist(userId, blackUserId);
		return checkResult(result);
	}
}
