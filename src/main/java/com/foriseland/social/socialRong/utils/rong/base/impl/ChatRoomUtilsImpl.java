package com.foriseland.social.socialRong.utils.rong.base.impl;

import com.foriseland.social.socialRong.utils.rong.base.BaseUtils;
import com.foriseland.social.socialRong.utils.rong.base.ChatRoomUtils;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.messages.BaseMessage;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.ChatRoomGlobalGagQueryResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.ChatRoomInfo;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.ChatRoomWhiteListUserResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.ChatroomPriorityResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.ChatroomQueryResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.ChatroomUserQueryResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.CodeSuccessResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.ListBlockChatroomUserResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.ListGagChatroomUserResult;

/**
 * 聊天室工具
 * @author sjm
 *
 */
public class ChatRoomUtilsImpl extends BaseUtils implements ChatRoomUtils{

	public String publishChatroom(String userId, String[] toChatroomId, BaseMessage message)
			throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.message.publishChatroom(userId, toChatroomId, message);
		return checkResult(result);
	}
	
	public String broadcast(String fromUserId, BaseMessage message) throws IllegalArgumentException, Exception {
		return broadcastExtendALL(fromUserId, message, null, null, null);
	}

	public String broadcastExtend(String fromUserId, BaseMessage message, String pushContent)
			throws IllegalArgumentException, Exception {
		return broadcastExtendALL(fromUserId, message, pushContent, null, null);
	}

	public String broadcastExtendIOS(String fromUserId, BaseMessage message, String pushContent, String pushData)
			throws IllegalArgumentException, Exception {
		return broadcastExtendALL(fromUserId, message, pushContent, pushData, null);
	}

	public String broadcastExtendOS(String fromUserId, BaseMessage message, String pushContent, String os)
			throws IllegalArgumentException, Exception {
		return broadcastExtendALL(fromUserId, message, pushContent, null, os);
	}

	public String broadcastExtendALL(String fromUserId, BaseMessage message, String pushContent, String pushData,
			String os) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.message.broadcast(fromUserId, message, pushContent, pushData, os);
		return checkResult(result);
	}

	public String create(ChatRoomInfo[] chatRoomInfo) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.chatroom.create(chatRoomInfo);
		return checkResult(result);
	}

	public String join(String[] userId, String chatroomId) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.chatroom.join(userId, chatroomId);
		return checkResult(result);
	}

	public String query(String[] chatroomId) throws IllegalArgumentException, Exception {
		ChatroomQueryResult result = rongCloud.chatroom.query(chatroomId);
		return checkResult(result);
	}

	public String queryUser(String chatroomId, String count, String order) throws IllegalArgumentException, Exception {
		ChatroomUserQueryResult result = rongCloud.chatroom.queryUser(chatroomId, count, order);
		return checkResult(result);
	}

	public String stopDistributionMessage(String chatroomId) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.chatroom.stopDistributionMessage(chatroomId);
		return checkResult(result);
	}

	public String resumeDistributionMessage(String chatroomId) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.chatroom.resumeDistributionMessage(chatroomId);
		return checkResult(result);
	}

	public String addGagUser(String userId, String chatroomId, String minute)
			throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.chatroom.addGagUser(userId, chatroomId, minute);
		return checkResult(result);
	}

	public String ListGagUser(String chatroomId) throws IllegalArgumentException, Exception {
		ListGagChatroomUserResult result = rongCloud.chatroom.ListGagUser(chatroomId);
		return checkResult(result);
	}

	public String rollbackGagUser(String userId, String chatroomId) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.chatroom.rollbackBlockUser(userId, chatroomId);
		return checkResult(result);
	}

	public String addBlockUser(String userId, String chatroomId, String minute)
			throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.chatroom.addBlockUser(userId, chatroomId, minute);
		return checkResult(result);
	}

	public String getListBlockUser(String chatroomId) throws IllegalArgumentException, Exception {
		ListBlockChatroomUserResult result = rongCloud.chatroom.getListBlockUser(chatroomId);
		return checkResult(result);
	}

	public String rollbackBlockUser(String userId, String chatroomId) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.chatroom.rollbackBlockUser(userId, chatroomId);
		return checkResult(result);
	}

	public String addPriority(String[] objectName) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.chatroom.addPriority(objectName);
		return checkResult(result);
	}

	public String destroy(String[] chatroomId) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.chatroom.destroy(chatroomId);
		return checkResult(result);
	}

	public String addWhiteListUser(String chatroomId, String[] userId) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.chatroom.addWhiteListUser(chatroomId, userId);
		return checkResult(result);
	}

	public String rollbackPriority(String[] objectName) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.chatroom.rollbackPriority(objectName);
		return checkResult(result);
	}

	public String queryPriority() throws IllegalArgumentException, Exception {
		ChatroomPriorityResult result = rongCloud.chatroom.queryPriority();
		return checkResult(result);
	}

	public String addGlobalGag(String[] userId, String minute) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.chatroom.addGlobalGag(userId, minute);
		return checkResult(result);
	}

	public String rollbackGlobalGag(String[] userId) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.chatroom.rollbackGlobalGag(userId);
		return checkResult(result);
	}

	public String queryGlobalGag() throws Exception {
		ChatRoomGlobalGagQueryResult result = rongCloud.chatroom.queryGlobalGag();
		return checkResult(result);
	}

	public String removeWhiteListUser(String chatroomId, String[] userId) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.chatroom.removeWhiteListUser(chatroomId, userId);
		return checkResult(result);
	}

	public String queryWhiteListUser(String chatroomId) throws IllegalArgumentException, Exception {
		ChatRoomWhiteListUserResult result = rongCloud.chatroom.queryWhiteListUser(chatroomId);
		return checkResult(result);
	}
	
}
