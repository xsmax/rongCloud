package com.foriseland.social.socialRong.utils.rong.base;

import com.foriseland.social.socialRong.utils.thirdparty.io.rong.messages.BaseMessage;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.GroupInfo;

public interface GroupUtils{
	//群组

		/**
		 * 发送群组消息方法（以一个用户身份向群组发送消息，单条消息最大 128k.每秒钟最多发送 20 条消息，每次最多向 3 个群组发送，如：一次向 3 个群组发送消息，示为 3 条消息。）
		 * @param fromUserId 发送人用户 Id 。（必传）
		 * @param toGroupId 接收群Id，提供多个本参数可以实现向多群发送消息，最多不超过 3 个群组。（必传）
		 * @param message 发送消息内容（必传）
		 * @return String
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String publishGroup(String fromUserId, String[] toGroupId, BaseMessage message) throws IllegalArgumentException,Exception;

		/**
		 * 发送群组消息方法（以一个用户身份向群组发送消息，单条消息最大 128k.每秒钟最多发送 20 条消息，每次最多向 3 个群组发送，如：一次向 3 个群组发送消息，示为 3 条消息。）
		 * @param fromUserId 发送人用户 Id 。（必传）
		 * @param toGroupId 接收群Id，提供多个本参数可以实现向多群发送消息，最多不超过 3 个群组。（必传）
		 * @param message 发送消息内容（必传）
		 * @param pushContent 定义显示的 Push 内容，如果 objectName 为融云内置消息类型时，则发送后用户一定会收到 Push 信息. 如果为自定义消息，则 pushContent 为自定义消息显示的 Push 内容，如果不传则用户不会收到 Push 通知。（可选）
		 * @param isPersisted 当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行存储，0 表示为不存储、 1 表示为存储，默认为 1 存储消息。（可选）
		 * @param isCounted 当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行未读消息计数，0 表示为不计数、 1 表示为计数，默认为 1 计数，未读消息数增加 1。（可选）
		 * @param isIncludeSender 发送用户自已是否接收消息，0 表示为不接收，1 表示为接收，默认为 0 不接收。（可选）
		 * @return
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String publishGroupExtend(String fromUserId, String[] toGroupId, BaseMessage message, String pushContent, Integer isPersisted, Integer isCounted, Integer isIncludeSender) throws IllegalArgumentException,Exception;
		
		/**
		 * 发送群组消息方法（以一个用户身份向群组发送消息，单条消息最大 128k.每秒钟最多发送 20 条消息，每次最多向 3 个群组发送，如：一次向 3 个群组发送消息，示为 3 条消息。）
		 * @param fromUserId 发送人用户 Id 。（必传）
		 * @param toGroupId 接收群Id，提供多个本参数可以实现向多群发送消息，最多不超过 3 个群组。（必传）
		 * @param message 发送消息内容（必传）
		 * @param pushContent 定义显示的 Push 内容，如果 objectName 为融云内置消息类型时，则发送后用户一定会收到 Push 信息. 如果为自定义消息，则 pushContent 为自定义消息显示的 Push 内容，如果不传则用户不会收到 Push 通知。（可选）
		 * @param pushData 针对 iOS 平台为 Push 通知时附加到 payload 中，Android 客户端收到推送消息时对应字段名为 pushData。（可选）
		 * @param isPersisted 当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行存储，0 表示为不存储、 1 表示为存储，默认为 1 存储消息。（可选）
		 * @param isCounted 当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行未读消息计数，0 表示为不计数、 1 表示为计数，默认为 1 计数，未读消息数增加 1。（可选）
		 * @param isIncludeSender 发送用户自已是否接收消息，0 表示为不接收，1 表示为接收，默认为 0 不接收。（可选）
		 * @return
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String publishGroupExtendIOS(String fromUserId, String[] toGroupId, BaseMessage message, String pushContent, String pushData, Integer isPersisted, Integer isCounted, Integer isIncludeSender) throws IllegalArgumentException,Exception;
		
		//讨论组

		
		/**
		 * 发送讨论组消息方法（以一个用户身份向讨论组发送消息，单条消息最大 128k，每秒钟最多发送 20 条消息.）
		 * @param fromUserId 发送人用户 Id。（必传）
		 * @param toDiscussionId 接收讨论组 Id。（必传）
		 * @param message 发送消息内容（必传）
		 * @return String
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String publishDiscussion(String fromUserId, String toDiscussionId, BaseMessage message) throws IllegalArgumentException,Exception;
		

		/**
		 * 发送讨论组消息方法（以一个用户身份向讨论组发送消息，单条消息最大 128k，每秒钟最多发送 20 条消息.）
		 * @param fromUserId 发送人用户 Id。（必传）
		 * @param toDiscussionId 接收讨论组 Id。（必传）
		 * @param message 发送消息内容（必传）
		 * @param pushContent 定义显示的 Push 内容，如果 objectName 为融云内置消息类型时，则发送后用户一定会收到 Push 信息. 如果为自定义消息，则 pushContent 为自定义消息显示的 Push 内容，如果不传则用户不会收到 Push 通知。（可选）
		 * @param isPersisted 当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行存储，0 表示为不存储、 1 表示为存储，默认为 1 存储消息.（可选）
		 * @param isCounted 当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行未读消息计数，0 表示为不计数、 1 表示为计数，默认为 1 计数，未读消息数增加 1。（可选）
		 * @param isIncludeSender 发送用户自已是否接收消息，0 表示为不接收，1 表示为接收，默认为 0 不接收。（可选）
		 * @return String
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String publishDiscussionExtend(String fromUserId, String toDiscussionId, BaseMessage message, String pushContent, Integer isPersisted, Integer isCounted, Integer isIncludeSender) throws IllegalArgumentException,Exception;
		
		
		/**
		 * 发送讨论组消息方法（以一个用户身份向讨论组发送消息，单条消息最大 128k，每秒钟最多发送 20 条消息.） 
		 * @param fromUserId 发送人用户 Id。（必传）
		 * @param toDiscussionId 接收讨论组 Id。（必传）
		 * @param message 发送消息内容（必传）
		 * @param pushContent 定义显示的 Push 内容，如果 objectName 为融云内置消息类型时，则发送后用户一定会收到 Push 信息. 如果为自定义消息，则 pushContent 为自定义消息显示的 Push 内容，如果不传则用户不会收到 Push 通知。（可选）
		 * @param pushData 针对 iOS 平台为 Push 通知时附加到 payload 中，Android 客户端收到推送消息时对应字段名为 pushData.（可选）
		 * @param isPersisted 当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行存储，0 表示为不存储、 1 表示为存储，默认为 1 存储消息.（可选）
		 * @param isCounted 当前版本有新的自定义消息，而老版本没有该自定义消息时，老版本客户端收到消息后是否进行未读消息计数，0 表示为不计数、 1 表示为计数，默认为 1 计数，未读消息数增加 1。（可选）
		 * @param isIncludeSender 发送用户自已是否接收消息，0 表示为不接收，1 表示为接收，默认为 0 不接收。（可选）
		 * @return String
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String publishDiscussionExtendIOS(String fromUserId, String toDiscussionId, BaseMessage message, String pushContent, String pushData, Integer isPersisted, Integer isCounted, Integer isIncludeSender) throws IllegalArgumentException,Exception;
		
		
		/**
		 * 创建群组方法（创建群组，并将用户加入该群组，用户将可以收到该群的消息，同一用户最多可加入 500 个群，每个群最大至 3000 人，App 内的群组数量没有限制.注：其实本方法是加入群组方法 /group/join 的别名。）
		 * @param userId 要加入群的用户 Id。（必传）
		 * @param groupId 创建群组 Id。（必传）
		 * @param groupName 群组 Id 对应的名称。（必传）
		 * @return
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String create(String[] userId, String groupId, String groupName) throws IllegalArgumentException,Exception;
		
		/**
		 * 同步用户所属群组方法（当第一次连接融云服务器时，需要向融云服务器提交 userId 对应的用户当前所加入的所有群组，此接口主要为防止应用中用户群信息同融云已知的用户所属群信息不同步。）
		 * @param userId 被同步群信息的用户 Id。（必传）
		 * @param groupInfo 该用户的群信息，如群 Id 已经存在，则不会刷新对应群组名称，如果想刷新群组名称请调用刷新群组信息方法。
		 * @return String
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String sync(String userId, GroupInfo[] groupInfo) throws IllegalArgumentException,Exception;
		

		/**
		 * 刷新群组信息方法 
		 * @param groupId 群组 Id。（必传）
		 * @param groupName 群名称。（必传）
		 * @return String
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String refresh(String groupId, String groupName) throws IllegalArgumentException,Exception;
		
		/**
		 * 将用户加入指定群组，用户将可以收到该群的消息，同一用户最多可加入 500 个群，每个群最大至 3000 人。 
		 * @param userId 要加入群的用户 Id，可提交多个，最多不超过 1000 个。（必传）
		 * @param groupId 要加入的群 Id。（必传）
		 * @param groupName 要加入的群 Id 对应的名称。（必传）
		 * @return String
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String join(String[] userId, String groupId, String groupName) throws IllegalArgumentException,Exception;
		
		/**
		 * 查询群成员方法 
		 * @param groupId 群组Id。（必传）
		 * @return String
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String queryUser(String groupId) throws IllegalArgumentException,Exception;
		
		/**
		 * 退出群组方法（将用户从群中移除，不再接收该群组的消息.） 
		 * @param userId 要退出群的用户 Id.（必传）
		 * @param groupId 要退出的群 Id.（必传）
		 * @return
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String quit(String[] userId, String groupId) throws IllegalArgumentException,Exception;
		
		/**
		 * 添加禁言群成员方法（在 App 中如果不想让某一用户在群中发言时，可将此用户在群组中禁言，被禁言用户可以接收查看群组中用户聊天信息，但不能发送消息。）
		 * @param userId 用户 Id。（必传）
		 * @param groupId 群组 Id。（必传）
		 * @param minute 禁言时长，以分钟为单位，最大值为43200分钟。（必传）
		 * @return String
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String addGagUser(String userId, String groupId, String minute) throws IllegalArgumentException,Exception;
		
		/**
		 *  查询被禁言群成员方法 
		 * @param groupId 群组Id。（必传）
		 * @return String
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String lisGagUser(String groupId) throws IllegalArgumentException,Exception;
		
		
		/**
		 * 移除禁言群成员方法 
		 * @param userId 用户Id。支持同时移除多个群成员（必传）
		 * @param groupId 群组Id。（必传）
		 * @return String
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String rollBackGagUser(String[] userId, String groupId) throws IllegalArgumentException,Exception;
		
		/**
		 * 解散群组方法。（将该群解散，所有用户都无法再接收该群的消息。） 
		 * @param userId 操作解散群的用户 Id。（必传）
		 * @param groupId 要解散的群 Id。（必传）
		 * @return String
		 * @throws IllegalArgumentException
		 * @throws Exception
		 */
		public String dismiss(String userId, String groupId) throws IllegalArgumentException,Exception;
}
