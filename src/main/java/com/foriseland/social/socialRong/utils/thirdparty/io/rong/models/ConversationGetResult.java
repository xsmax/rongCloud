package com.foriseland.social.socialRong.utils.thirdparty.io.rong.models;

import com.foriseland.social.socialRong.utils.thirdparty.io.rong.util.GsonUtil;

public class ConversationGetResult {
	// 返回码，200 为正常。
	Integer code;
	// 消息免打扰设置状态，0 表示为关闭，1 表示为开启。
	Integer isMuted;
	// 错误信息。
	String errorMessage;
	
	
	public ConversationGetResult(Integer code, Integer isMuted, String errorMessage) {
		super();
		this.code = code;
		this.isMuted = isMuted;
		this.errorMessage = errorMessage;
	}
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public Integer getIsMuted() {
		return isMuted;
	}
	public void setIsMuted(Integer isMuted) {
		this.isMuted = isMuted;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		return GsonUtil.toJson(this, ConversationGetResult.class);
	}
	
	
}
