package com.foriseland.social.socialRong.utils.thirdparty.io.rong.models;

import java.util.List;

import com.foriseland.social.socialRong.utils.thirdparty.io.rong.util.GsonUtil;

public class ChatRoomGlobalGagQueryResult {
	// 返回码，200 为正常。
	Integer code;
	// 聊天室用户数组。
	List<ChatRoomUser> chatRoomUsers;
	// 错误信息。
	String errorMessage;
	
	public ChatRoomGlobalGagQueryResult(Integer code, List<ChatRoomUser> chatRoomUsers, String errorMessage) {
		super();
		this.code = code;
		this.chatRoomUsers = chatRoomUsers;
		this.errorMessage = errorMessage;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public List<ChatRoomUser> getChatRoomUsers() {
		return chatRoomUsers;
	}

	public void setChatRoomUsers(List<ChatRoomUser> chatRoomUsers) {
		this.chatRoomUsers = chatRoomUsers;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	@Override
	public String toString() {
		return GsonUtil.toJson(this, ChatRoomGlobalGagQueryResult.class);
	}
	
}
