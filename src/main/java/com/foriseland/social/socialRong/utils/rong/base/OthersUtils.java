package com.foriseland.social.socialRong.utils.rong.base;

import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.PushMessage;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.UserTag;

public interface OthersUtils {
	
	/**
	 * 添加敏感词方法（设置敏感词后，App 中用户不会收到含有敏感词的消息内容，默认最多设置 50 个敏感词。） 
	 * @param word  敏感词，最长不超过 32 个字符。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String add(String word) throws IllegalArgumentException,Exception;
	
	/**
	 * 查询敏感词列表方法 
	 * @return String
	 * @throws Exception
	 */
	public String getList() throws Exception;
	
	/**
	 * 移除敏感词方法（从敏感词列表中，移除某一敏感词。） 
	 * @param word 敏感词，最长不超过 32 个字符。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String delete(String word) throws IllegalArgumentException,Exception;
	
	/**
	 * 批量移除敏感詞方法 
	 * @param words 从敏感词列表中，批量移除某些敏感词，一次最多移除敏感词不超过 50 个，移除后 2 小时内生效。(必傳)
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String batchDelete(String[] words) throws IllegalArgumentException,Exception;
	
	/**
	 * 添加 Push 标签方法 
	 * @param userTag 用户标签。
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String setUserPushTag(UserTag userTag) throws IllegalArgumentException,Exception;
	
	/**
	 * 广播消息方法（fromuserid 和 message为null即为不落地的push） SDK 中会话类型为 SYSTEM
	 * 推送 方法  SDK 中会话类型为 PUSH_SERVICE
	 * @param pushMessage
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String broadcastPush(PushMessage pushMessage) throws IllegalArgumentException,Exception;
	/**
	 * 获取图片验证码方法
	 * @return String
	 * @throws Exception
	 */
	public String getImageCode() throws Exception;
	
	/**
	 * 发送短信验证码方法。
	 * @param mobile 接收短信验证码的目标手机号，每分钟同一手机号只能发送一次短信验证码，同一手机号 1 小时内最多发送 3 次。（必传）
	 * @param templateId 短信模板 Id，在开发者后台->短信服务->服务设置->短信模版中获取。（必传）
	 * @param region 手机号码所属国家区号，目前只支持中图区号 86）
	 * @param verifyId 图片验证标识 Id ，开启图片验证功能后此参数必传，否则可以不传。在获取图片验证码方法返回值中获取。
	 * @param verifyCode 图片验证码，开启图片验证功能后此参数必传，否则可以不传。
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String sendCode(String mobile, String templateId, String region, String verifyId, String verifyCode) throws IllegalArgumentException,Exception;
	/**
	 * 验证码验证方法 
	 * @param sessionId 短信验证码唯一标识，在发送短信验证码方法，返回值中获取。（必传）
	 * @param code 短信验证码内容。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String verifyCode(String sessionId, String code) throws IllegalArgumentException,Exception;
	
	
}
