package com.foriseland.social.socialRong.utils.rong.base;

/**
 * 用户工具抽象类
 * @author sjm
 *
 */
public interface UserUtils{
	/**
	 * 获取 Token 方法 
	 * @param userId 用户 Id，最大长度 64 字节.是用户在 App 中的唯一标识码，必须保证在同一个 App 内不重复，重复的用户 Id 将被当作是同一用户。（必传）
	 * @param userName 用户名称，最大长度 128 字节.用来在 Push 推送时显示用户的名称.用户名称，最大长度 128 字节.用来在 Push 推送时显示用户的名称。（必传）
	 * @param portraitUri 用户头像 URI，最大长度 1024 字节.用来在 Push 推送时显示用户的头像。（必传）
	 * @return string
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String getToken(String userId,String userName,String portraitUri) throws IllegalArgumentException,Exception;
	
	/**
	 * 刷新用户信息方法 
	 * @param userId 用户 Id，最大长度 64 字节.是用户在 App 中的唯一标识码，必须保证在同一个 App 内不重复，重复的用户 Id 将被当作是同一用户。（必传）
	 * @param userName 用户名称，最大长度 128 字节。用来在 Push 推送时，显示用户的名称，刷新用户名称后 5 分钟内生效。（可选，提供即刷新，不提供忽略）
	 * @param portraitUri 用户头像 URI，最大长度 1024 字节。用来在 Push 推送时显示。（可选，提供即刷新，不提供忽略）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String refresh(String userId,String userName,String portraitUri) throws IllegalArgumentException,Exception;

	/**
	 * 检查用户在线状态 方法 
	 * @param userId 用户 Id，最大长度 64 字节。是用户在 App 中的唯一标识码，必须保证在同一个 App 内不重复，重复的用户 Id 将被当作是同一用户。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String checkOnline(String userId) throws IllegalArgumentException, Exception;

	/**
	 * 封禁用户方法（每秒钟限 100 次） 
	 * @param userId userId:用户 Id。（必传）
	 * @param minute minute:封禁时长,单位为分钟，最大值为43200分钟。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String block(String userId, Integer minute) throws IllegalArgumentException, Exception;

	/**
	 * 解除用户封禁方法（每秒钟限 100 次） 
	 * @param userId 用户 Id。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String unBlock(String userId) throws IllegalArgumentException, Exception;

	/**
	 * 获取被封禁用户方法（每秒钟限 100 次） 
	 * @return String
	 * @throws Exception
	 */
	public String queryBlock() throws Exception;

	/**
	 * 添加用户到黑名单方法（每秒钟限 100 次） 
	 * @param userId 用户 Id。（必传）
	 * @param blackUserId 被加到黑名单的用户Id。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String addBlacklist(String userId, String blackUserId) throws IllegalArgumentException, Exception;

	/**
	 * 获取某用户的黑名单列表方法（每秒钟限 100 次） 
	 * @param userId 用户 Id。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String queryBlacklist(String userId) throws IllegalArgumentException, Exception;

	/**
	 * 从黑名单中移除用户方法（每秒钟限 100 次）
	 * @param userId 用户 Id。（必传）
	 * @param blackUserId 被移除的用户Id。（必传）
	 * @return String
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public String removeBlacklist(String userId, String blackUserId) throws IllegalArgumentException, Exception;
	
}
