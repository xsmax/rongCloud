package com.foriseland.social.socialRong.utils.rong.base;

import com.foriseland.social.socialRong.config.ConfigFactory;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.RongCloud;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.messages.BaseMessage;

/**
 * 融云基类初始化参数
 * @author sjm
 * 
 */
public abstract class BaseUtils {
	protected static ConfigFactory conf = null;
	protected static RongCloud rongCloud = null;
	
	/*
	 * 初始化融云
	 */
	static{
		conf = new ConfigFactory();
		conf.load();
		rongCloud = RongCloud.getInstance(conf.getString("rong.cloud.appKey"),conf.getString("rong.cloud.appSecret"));
	}

	/**
	 * 检测对象是否为空
	 * @param object
	 * @return
	 */
	protected String checkResult(Object object){
		if(null != object){
			return object.toString();
		}else{
			return "";	
		}
	}
}
