package com.foriseland.social.socialRong.utils.thirdparty.io.rong.models;

import java.util.List;

import com.foriseland.social.socialRong.utils.thirdparty.io.rong.util.GsonUtil;

/**
 * ChatroomPriorityResult 返回結果
 * @author sjm
 *
 */
public class ChatroomPriorityResult {
	
	// 返回码，200 为正常。
	Integer code;
	// 聊天室信息数组。
	List<String> msgType;
	// 错误信息。
	String errorMessage;
	
	public ChatroomPriorityResult(Integer code, List<String> msgType, String errorMessage) {
		super();
		this.code = code;
		this.msgType = msgType;
		this.errorMessage = errorMessage;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public List<String> getMsgType() {
		return msgType;
	}

	public void setMsgType(List<String> msgType) {
		this.msgType = msgType;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		return GsonUtil.toJson(this, ChatroomPriorityResult.class);
	}
	
}
