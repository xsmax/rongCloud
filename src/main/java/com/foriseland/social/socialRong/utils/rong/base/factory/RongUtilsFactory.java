package com.foriseland.social.socialRong.utils.rong.base.factory;

import com.foriseland.social.socialRong.utils.rong.base.ChatRoomUtils;
import com.foriseland.social.socialRong.utils.rong.base.GroupUtils;
import com.foriseland.social.socialRong.utils.rong.base.MessageUtils;
import com.foriseland.social.socialRong.utils.rong.base.OthersUtils;
import com.foriseland.social.socialRong.utils.rong.base.PeopleUtils;
import com.foriseland.social.socialRong.utils.rong.base.UserUtils;
import com.foriseland.social.socialRong.utils.rong.base.impl.ChatRoomUtilsImpl;
import com.foriseland.social.socialRong.utils.rong.base.impl.GroupUtilsImpl;
import com.foriseland.social.socialRong.utils.rong.base.impl.MessageUtilsImpl;
import com.foriseland.social.socialRong.utils.rong.base.impl.OthersUtilsImpl;
import com.foriseland.social.socialRong.utils.rong.base.impl.PeopleUtilsImpl;
import com.foriseland.social.socialRong.utils.rong.base.impl.UserUtilsImpl;

/**
 * 工具工厂类
 * @author pc
 *
 */
public class RongUtilsFactory {
	static UserUtils userUtils = null;	
	static ChatRoomUtils chatRoomUtils = null;
	static GroupUtils groupUtils = null;
	static MessageUtils messageUtils  = null;
	static OthersUtils othersUtils =null;
	static PeopleUtils peopleUtils = null;	
	
	static {
		userUtils = new UserUtilsImpl();
		chatRoomUtils = new ChatRoomUtilsImpl();
		groupUtils = new GroupUtilsImpl();
		messageUtils = new MessageUtilsImpl();
		othersUtils = new OthersUtilsImpl();
		peopleUtils = new PeopleUtilsImpl();
	}
	
	public static UserUtils buildUserUtils(){
		return userUtils;
	}
	public UserUtils getUserUtils(){
		return userUtils;
	}
	public static ChatRoomUtils getChatRoomUtils(){
		return chatRoomUtils;
	}
	public static ChatRoomUtils buildChatRoomUtils(){
		return chatRoomUtils;
	}
	public static GroupUtils getGroupUtils(){
		return groupUtils;
	}
	public static GroupUtils buildGroupUtils(){
		return groupUtils;
	}
	public static MessageUtils getMessageUtils(){
		return messageUtils;
	}
	public static MessageUtils buildMessageUtils(){
		return messageUtils;
	}
	public static OthersUtils getOtherUtils(){
		return othersUtils;
	}
	public static OthersUtils buildOtherUtils(){
		return othersUtils;
	}
	public static PeopleUtils getPeopleUtils(){
		return peopleUtils;
	}
	public static RongUtilsFactory build(){
		return new RongUtilsFactory();
	}
}
