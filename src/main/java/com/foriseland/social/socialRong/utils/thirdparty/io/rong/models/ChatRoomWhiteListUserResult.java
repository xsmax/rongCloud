package com.foriseland.social.socialRong.utils.thirdparty.io.rong.models;

import java.util.List;

import com.foriseland.social.socialRong.utils.thirdparty.io.rong.util.GsonUtil;

public class ChatRoomWhiteListUserResult {
	// 返回码，200 为正常。
	Integer code;
	// 白名单用户数组。
	List<String> users;
	// 错误信息。
	String errorMessage;
	public ChatRoomWhiteListUserResult(Integer code, List<String> users, String errorMessage) {
		super();
		this.code = code;
		this.users = users;
		this.errorMessage = errorMessage;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public List<String> getUsers() {
		return users;
	}
	public void setUsers(List<String> users) {
		this.users = users;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	@Override
	public String toString() {
		return GsonUtil.toJson(this, ChatRoomWhiteListUserResult.class);
	}
	
}
