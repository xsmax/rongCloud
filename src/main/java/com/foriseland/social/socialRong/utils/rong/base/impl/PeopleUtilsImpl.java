package com.foriseland.social.socialRong.utils.rong.base.impl;

import com.foriseland.social.socialRong.utils.rong.base.BaseUtils;
import com.foriseland.social.socialRong.utils.rong.base.PeopleUtils;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.messages.BaseMessage;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.CodeSuccessResult;

/**
 * 個人工具
 * @author sjm
 *
 */
public class PeopleUtilsImpl extends BaseUtils implements PeopleUtils{

	public String publishPrivate(String userId, String[] messagePublishPrivateToUserId, BaseMessage message)
			throws IllegalArgumentException, Exception {
		return publishPrivateExtendIOS(userId,messagePublishPrivateToUserId,message,null,null,null,null,null,null,null);
	}

	public String publishPrivateExtend(String fromUserId, String[] toUserId, BaseMessage message, String pushContent,
			Integer verifyBlacklist, Integer isPersisted, Integer isCounted, Integer isIncludeSender)
			throws IllegalArgumentException, Exception {
		return publishPrivateExtendIOS(fromUserId,toUserId,message,pushContent,null,null,verifyBlacklist,isCounted,isCounted,isIncludeSender);
	}

	public String publishPrivateExtendIOS(String fromUserId, String[] toUserId, BaseMessage message, String pushContent,
			String pushData, String count, Integer verifyBlacklist, Integer isPersisted, Integer isCounted,
			Integer isIncludeSender) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.message.publishPrivate(fromUserId,toUserId,message,pushContent,pushData,count,verifyBlacklist,isPersisted,isCounted,isIncludeSender);
		return checkResult(result);
	}

	public String PublishSystem(String fromUserId, String[] toUserId, BaseMessage message) throws IllegalArgumentException,Exception {
		return PublishSystemExtendIOS(fromUserId, toUserId, message, null, null, null, null);
	}

	public String PublishSystemExtend(String fromUserId, String[] toUserId, BaseMessage message, String pushContent,
			Integer isPersisted, Integer isCounted) throws IllegalArgumentException,Exception {
		return PublishSystemExtendIOS(fromUserId, toUserId, message, pushContent, null, isPersisted, isCounted);
	}

	public String PublishSystemExtendIOS(String fromUserId, String[] toUserId, BaseMessage message, String pushContent,
			String pushData, Integer isPersisted, Integer isCounted) throws IllegalArgumentException,Exception {
		CodeSuccessResult result = rongCloud.message.PublishSystem(pushData, toUserId, message, pushData, pushData, isCounted, isCounted);
		return checkResult(result);
	}

}
