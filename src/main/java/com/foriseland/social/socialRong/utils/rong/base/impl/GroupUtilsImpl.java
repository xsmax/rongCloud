package com.foriseland.social.socialRong.utils.rong.base.impl;

import com.foriseland.social.socialRong.utils.rong.base.BaseUtils;
import com.foriseland.social.socialRong.utils.rong.base.GroupUtils;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.messages.BaseMessage;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.CodeSuccessResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.GroupInfo;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.GroupUserQueryResult;
import com.foriseland.social.socialRong.utils.thirdparty.io.rong.models.ListGagGroupUserResult;

/**
 * 群组工具
 * 
 * @author sjm
 *
 */
public class GroupUtilsImpl extends BaseUtils implements GroupUtils {

	public String publishGroup(String fromUserId, String[] toGroupId, BaseMessage message)
			throws IllegalArgumentException, Exception {
		return publishGroupExtendIOS(fromUserId, toGroupId, message, null, null, null, null, null);
	}

	public String publishGroupExtend(String fromUserId, String[] toGroupId, BaseMessage message, String pushContent,
			Integer isPersisted, Integer isCounted, Integer isIncludeSender)
			throws IllegalArgumentException, Exception {
		return publishGroupExtendIOS(fromUserId, toGroupId, message, pushContent, null, isPersisted, isCounted, isIncludeSender);
	}


	public String publishGroupExtendIOS(String fromUserId, String[] toGroupId, BaseMessage message, String pushContent,
			String pushData, Integer isPersisted, Integer isCounted, Integer isIncludeSender)
			throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.message.publishGroup(fromUserId, toGroupId, message, pushContent, pushData, isPersisted, isCounted, isIncludeSender);
		return checkResult(result);
	}


	public String publishDiscussion(String fromUserId, String toDiscussionId, BaseMessage message)
			throws IllegalArgumentException, Exception {
		return publishDiscussionExtendIOS(fromUserId, toDiscussionId, message, null, null, null, null, null);
	}

	public String publishDiscussionExtend(String fromUserId, String toDiscussionId, BaseMessage message,
			String pushContent, Integer isPersisted, Integer isCounted, Integer isIncludeSender)
			throws IllegalArgumentException, Exception {
		return publishDiscussionExtendIOS(fromUserId, toDiscussionId, message, pushContent, null, isPersisted, isCounted, isIncludeSender);
	}


	public String publishDiscussionExtendIOS(String fromUserId, String toDiscussionId, BaseMessage message,
			String pushContent, String pushData, Integer isPersisted, Integer isCounted, Integer isIncludeSender)
			throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.message.publishDiscussion(fromUserId, toDiscussionId, message, pushContent, pushData, isPersisted, isCounted, isIncludeSender);
		return checkResult(result);
	}

	public String create(String[] userId, String groupId, String groupName) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.group.create(userId, groupId, groupName);
		return checkResult(result);
	}

	public String sync(String userId, GroupInfo[] groupInfo) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.group.sync(userId, groupInfo);
		return checkResult(result);
	}

	public String refresh(String groupId, String groupName) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.group.refresh(groupId, groupName);
		return checkResult(result);
	}

	public String join(String[] userId, String groupId, String groupName) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.group.join(userId, groupId, groupName);
		return checkResult(result);
	}

	public String queryUser(String groupId) throws IllegalArgumentException, Exception {
		GroupUserQueryResult result = rongCloud.group.queryUser(groupId);
		return checkResult(result);
	}

	public String quit(String[] userId, String groupId) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.group.quit(userId, groupId);
		return checkResult(result);
	}

	public String addGagUser(String userId, String groupId, String minute) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.group.addGagUser(userId, groupId, minute);
		return checkResult(result);
	}

	public String lisGagUser(String groupId) throws IllegalArgumentException, Exception {
		ListGagGroupUserResult result = rongCloud.group.lisGagUser(groupId);
		return checkResult(result);
	}

	public String rollBackGagUser(String[] userId, String groupId) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.group.rollBackGagUser(userId, groupId);
		return checkResult(result);
	}

	public String dismiss(String userId, String groupId) throws IllegalArgumentException, Exception {
		CodeSuccessResult result = rongCloud.group.dismiss(userId, groupId);
		return checkResult(result);
	}

}
